package epam.university.view;

import epam.university.model.bouquet.BouquetDecorationElement;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import java.util.List;

public class View {

  public void printObject(Object o) {
    System.out.println(Color.BLACK_BOLD + "\t\t" + o + Color.RESET);
  }

  public void printMenu(List<Object> menu, String prompt) {
    System.out.print("\n");
    int i = 1;
    for (Object o : menu) {
      System.out.print(Color.MAGENTA_BOLD + "\t[" + i + "] " + o + "\n");
      i++;
    }
    System.out.print(Color.MAGENTA_BOLD + "\t[q] Quit");
    System.out.print("\n" + prompt + ": " + Color.RESET);
  }

  public void printBouquets(List<BouquetEnum> bouquets) {
    System.out.print("\n");
    int i = 1;
    for (BouquetEnum b : bouquets) {
      System.out.print(Color.MAGENTA_BOLD +"\t[" + i + "] " + b + "\n");
      i++;
    }
    System.out.print(Color.MAGENTA_BOLD + "\t[" + i + "] Create custom bouquet\n");
    System.out.print("\ninput: " + Color.RESET);
  }

  public void printFlowers(List<Flower> flowers) {
    System.out.print("\n");
    int i = 1;
    for (Flower f : flowers) {
      System.out.print(Color.MAGENTA_BOLD +"\t[" + i + "] " + f + "\n");
      i++;
    }
    System.out.print("\t[" + i + "] Go to decoration\n");
    System.out.print("\ninput: "+ Color.RESET);
  }

  public void printDecorationElements(List<BouquetDecorationElement> decorationElements) {
    System.out.print("\n");
    int i = 1;
    for (BouquetDecorationElement b : decorationElements) {
      System.out.print(Color.MAGENTA_BOLD +"\t[" + i + "] " + b + "\n");
      i++;
    }
    System.out.print("\t[" + i + "] finish\n");
    System.out.print("\ninput: "+ Color.RESET);
  }

  public void printNotification(Object notification) {
    System.out.println(Color.BLUE_BOLD + "\nClient Notification:");
    System.out.println("\t\t\t\t" + notification + Color.RESET);
  }
}
