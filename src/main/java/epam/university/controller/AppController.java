package epam.university.controller;

import epam.university.model.client.Client;
import epam.university.model.client.SystemClient;
import epam.university.model.discount.impl.NewClientCard;
import epam.university.model.order.Order;
import epam.university.view.View;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class AppController {

  private Scanner scn;
  private String userInput;
  private View view;
  private Order order;

  public AppController() {
    scn = new Scanner(System.in);
    view = new View();
    order = new Order();
  }

  public void start() {
    initClient();
    System.out.println("---Going to state Bouquet Creation---");
    order.bouquetCreation();
    if (order.getBouquet().getBouquetDecorationElements().size() == 0) {
      order.bouquetDecoration();
    }
    view.printObject(order.getBouquet());
    System.out.println("---Going to state Register Order---");
    order.registerOrder();
    view.printObject(order);
    order.deliveringOrder();
  }

  private void initClient() {
    view.printMenu(Arrays.asList("New user", "Sign in"), "input");
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    if (userInput.equals("1")) {
      Client client = new Client();
      client.setDiscountCard(new NewClientCard());
      order.setClient(client);
    } else if (userInput.equals("2")) {
      handleSignIn();
    }
  }

  private void handleSignIn() {
    boolean userFound = false;
    SystemClient signedInClient = null;
    while (!userFound) {
      System.out.print("\nEnter your phone number: ");
      userInput = scn.nextLine();
      try {
        signedInClient = Arrays.stream(SystemClient.values())
            .filter(u -> userInput.equals(u.getPhoneNumber()))
            .findFirst().get();
      } catch (NoSuchElementException e) {
        System.err.println(e);
        continue;
      }
      System.out.println("\nWelcome!");
      userFound = true;
      }
    Client client = new Client();
    client.setDiscountCard(signedInClient.getDiscountCardType());
    order.setClient(client);
    }
}

