package epam.university.model.observer.promoter;

import epam.university.model.observer.Observable;
import epam.university.model.observer.ObserverSubscriber;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

public class Promoter implements Observable {

  private List<String> promotions;
  private final String promotionsSource = "src\\main\\resources\\promotions.properties";
  private String currentPromotion;
  private List<ObserverSubscriber> subscribers;

  private final Random random = new Random();

  public Promoter() {
    promotions = new LinkedList<>();
    initPromotions(promotionsSource);
    subscribers = new LinkedList<>();
  }

  private void initPromotions(String source) {
    try (InputStream input = new FileInputStream(promotionsSource)) {
      Properties prop = new Properties();
      prop.load(input);
      Integer i = 1;
      while (prop.containsKey(i.toString())) {
        promotions.add(prop.getProperty(i.toString()));
        i++;
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  public String getPromotion() {
    return currentPromotion = promotions.get(random.nextInt(promotions.size()));
  }

  @Override
  public void add(ObserverSubscriber observerSubscriber) {
    subscribers.add(observerSubscriber);
  }

  @Override
  public void remove(ObserverSubscriber observerSubscriber) {
    subscribers.remove(observerSubscriber);
  }

  @Override
  public void notifySubscribers() {
    subscribers.forEach(s -> s.update(currentPromotion));
  }
}
