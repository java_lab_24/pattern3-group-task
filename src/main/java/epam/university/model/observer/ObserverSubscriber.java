package epam.university.model.observer;

public interface ObserverSubscriber {

  void update(Object massage);
}
