package epam.university.model.observer.delivery;

import epam.university.model.observer.Observable;
import epam.university.model.observer.ObserverSubscriber;
import java.util.LinkedList;
import java.util.List;

public class DeliveryNotifier implements Observable {

  private List<ObserverSubscriber> subscribers;

  public DeliveryNotifier() {
    subscribers = new LinkedList<>();
  }

  @Override
  public void add(ObserverSubscriber observerSubscriber) {
    subscribers.add(observerSubscriber);
  }

  @Override
  public void remove(ObserverSubscriber observerSubscriber) {
    subscribers.remove(observerSubscriber);
  }

  @Override
  public void notifySubscribers() {
    subscribers.forEach(s -> s.update("Your order has delivered and waiting for you..."));
  }
}
