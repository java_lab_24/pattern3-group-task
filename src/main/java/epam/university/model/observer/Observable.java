package epam.university.model.observer;

public interface Observable {

  void add(ObserverSubscriber observerSubscriber);

  void remove(ObserverSubscriber observerSubscriber);

  void notifySubscribers();
}
