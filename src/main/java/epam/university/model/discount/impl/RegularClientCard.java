package epam.university.model.discount.impl;

import epam.university.model.discount.DiscountCardType;

public class RegularClientCard extends DiscountCardType {

  private final double DISCOUNT = 10;
  private final String TO_NAME = " + RegularClientCard";

  @Override
  public double getPercentage() {
    return DISCOUNT;
  }

  @Override
  public int getBonuses() {
    return 0;
  }

  public String getName() {
    return TO_NAME;
  }

}
