package epam.university.model.discount.impl;

import epam.university.model.discount.DiscountCardType;

public class BonusCard extends DiscountCardType {

  private int bonus = 20;
  private final String TO_NAME = " + used bonuses";

  @Override
  public double getPercentage() {
    return 0;
  }

  @Override
  public int getBonuses() {
    return bonus;
  }

  public String getName() {
    return TO_NAME;
  }

}
