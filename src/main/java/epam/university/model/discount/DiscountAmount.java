package epam.university.model.discount;

import epam.university.model.order.IOrder;

public interface DiscountAmount {

  double getDiscount(IOrder order);
}
