package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class TransparentPackagingWrap extends BouquetDecorationElement {
    private final double DECORATION_COST = 24;

    public TransparentPackagingWrap() {
        DECORATION_NAME = "Transparent Packaging Wrap";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
