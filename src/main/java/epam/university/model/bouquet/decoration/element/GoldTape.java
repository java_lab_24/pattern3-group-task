package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class GoldTape extends BouquetDecorationElement {
    private final double DECORATION_COST = 18;

    public GoldTape() {
        DECORATION_NAME = "Gold Tape";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
