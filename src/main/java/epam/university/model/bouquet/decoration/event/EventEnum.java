package epam.university.model.bouquet.decoration.event;

public enum EventEnum {
    WEDDING, BIRTHDAY, FUNERAL, VALENTINEDAY;
}
