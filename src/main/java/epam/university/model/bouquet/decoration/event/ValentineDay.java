package epam.university.model.bouquet.decoration.event;

import epam.university.model.bouquet.IBouquet;
import epam.university.model.bouquet.decoration.element.BlackTape;
import epam.university.model.bouquet.decoration.element.RedTape;
import epam.university.model.bouquet.decoration.element.PinkPackingPaper;

public class ValentineDay implements Event{
    private RedTape redTapeDecoration;
    private BlackTape blackTapeDecoration;
    private PinkPackingPaper pinkPackingPaper;

    public ValentineDay() {
        redTapeDecoration = new RedTape();
        blackTapeDecoration = new BlackTape();
        pinkPackingPaper = new PinkPackingPaper();
    }

    public IBouquet decorate(IBouquet IBouquet) {
        redTapeDecoration.setBouquet(IBouquet);
        blackTapeDecoration.setBouquet(redTapeDecoration);
        pinkPackingPaper.setBouquet(blackTapeDecoration);
        return pinkPackingPaper;
    }
}
