package epam.university.model.bouquet.decoration.event;

import epam.university.model.bouquet.IBouquet;
import epam.university.model.bouquet.decoration.element.GoldTape;
import epam.university.model.bouquet.decoration.element.GreenPackingPaper;
import epam.university.model.bouquet.decoration.element.WhitePackingPaper;

public class Birthday implements Event{
    private GreenPackingPaper greenPackingPaper;
    private WhitePackingPaper whitePackingPaper;
    private GoldTape goldTapeDecoration;

    public Birthday() {
        goldTapeDecoration = new GoldTape();
        whitePackingPaper = new WhitePackingPaper();
        greenPackingPaper = new GreenPackingPaper();
    }

    public IBouquet decorate(IBouquet IBouquet) {
        greenPackingPaper.setBouquet(IBouquet);
        whitePackingPaper.setBouquet(greenPackingPaper);
        goldTapeDecoration.setBouquet(whitePackingPaper);
        return goldTapeDecoration;
    }
}
