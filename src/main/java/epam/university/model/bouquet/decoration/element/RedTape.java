package epam.university.model.bouquet.decoration.element;

import epam.university.model.bouquet.BouquetDecorationElement;

public class RedTape extends BouquetDecorationElement {

    private final double DECORATION_COST = 13;

    public RedTape() {
        DECORATION_NAME = "Red Tape";
        setUsedDecorationElementCost(DECORATION_COST);
        setUsedDecorationElement(this);
    }
}
