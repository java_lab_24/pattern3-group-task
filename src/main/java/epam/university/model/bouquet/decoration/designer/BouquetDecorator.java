package epam.university.model.bouquet.decoration.designer;

import epam.university.model.bouquet.BouquetDecorationElement;
import epam.university.model.bouquet.IBouquet;
import epam.university.model.bouquet.decoration.element.BlackTape;
import epam.university.model.bouquet.decoration.element.DefaultTape;
import epam.university.model.bouquet.decoration.element.GoldTape;
import epam.university.model.bouquet.decoration.element.GreenPackingPaper;
import epam.university.model.bouquet.decoration.element.PinkPackingPaper;
import epam.university.model.bouquet.decoration.element.RedTape;
import epam.university.model.bouquet.decoration.element.TransparentPackagingWrap;
import epam.university.model.bouquet.decoration.element.WhitePackingPaper;
import epam.university.model.bouquet.decoration.event.Birthday;
import epam.university.model.bouquet.decoration.event.Event;
import epam.university.model.bouquet.decoration.event.EventEnum;
import epam.university.model.bouquet.decoration.event.Funeral;
import epam.university.model.bouquet.decoration.event.ValentineDay;
import epam.university.model.bouquet.decoration.event.Wedding;
import java.util.LinkedList;
import java.util.List;

public class BouquetDecorator {

  private List<BouquetDecorationElement> decorationElements;

  public BouquetDecorator() {
    decorationElements = new LinkedList<>();
    decorationElements.add(new BlackTape());
    decorationElements.add(new DefaultTape());
    decorationElements.add(new GoldTape());
    decorationElements.add(new RedTape());
    decorationElements.add(new GreenPackingPaper());
    decorationElements.add(new PinkPackingPaper());
    decorationElements.add(new WhitePackingPaper());
    decorationElements.add(new TransparentPackagingWrap());
  }

  public List<BouquetDecorationElement> getDecorationElements() {
    return decorationElements;
  }

  public IBouquet decorate(IBouquet IBouquet, EventEnum event) {
    Event decorateForEvent = null;
    switch (event) {
      case FUNERAL: {
        decorateForEvent = new Funeral();
        break;
      }
      case WEDDING: {
        decorateForEvent = new Wedding();
        break;
      }
      case VALENTINEDAY: {
        decorateForEvent = new ValentineDay();
        break;
      }
      default: {
        decorateForEvent = new Birthday();
      }
    }
    //return new Style.decorate(bouquet);
    return decorateForEvent.decorate(IBouquet);
  }

  public IBouquet decorate(IBouquet IBouquet, List<BouquetDecorationElement> decorationElements) {
    IBouquet decorated = IBouquet;
    for (BouquetDecorationElement decorationElement : decorationElements) {
      decorationElement.setBouquet(decorated);
      decorated = decorationElement;
    }
    return decorated;
  }
}
