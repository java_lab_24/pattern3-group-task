package epam.university.model.bouquet.factory;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import java.util.List;

public abstract class BouquetMarketFactory {

  protected List<BouquetEnum> availableBouquets;

  public abstract Bouquet getBouquet(BouquetEnum bouquetFromCatalog);

  public abstract Bouquet getBouquet(List<Flower> list);

  public List<BouquetEnum> getAvailableBouquets() {
    return availableBouquets;
  }
}
