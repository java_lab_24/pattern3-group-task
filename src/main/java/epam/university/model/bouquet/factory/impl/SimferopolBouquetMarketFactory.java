package epam.university.model.bouquet.factory.impl;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import epam.university.model.bouquet.factory.BouquetMarketFactory;
import java.util.LinkedList;
import java.util.List;

public class SimferopolBouquetMarketFactory extends BouquetMarketFactory {

    public SimferopolBouquetMarketFactory() {
        availableBouquets = new LinkedList<BouquetEnum>();
        availableBouquets.add(BouquetEnum.JUST_FOR_YOU);
        availableBouquets.add(BouquetEnum.FLORAL_FANTASY);
        availableBouquets.add(BouquetEnum.PINK_PERFECTION);
        availableBouquets.add(BouquetEnum.SPRING_BEAUTY);
        availableBouquets.add(BouquetEnum.DREAMLAND);
    }

    public Bouquet getBouquet(BouquetEnum bouquetFromCatalog){
        Bouquet bouquet = new Bouquet(bouquetFromCatalog.getFlowers());
        bouquet.setBouquetDecorationElements(bouquetFromCatalog.getDecorationElements());
        return bouquet;
    }

    public Bouquet getBouquet(List<Flower> flowerEnums){  // -> list Of Enams - Flower
        return new Bouquet(flowerEnums);
    }

    public List<BouquetEnum> getAvailableBouquets() {
        return availableBouquets;
    }
}
