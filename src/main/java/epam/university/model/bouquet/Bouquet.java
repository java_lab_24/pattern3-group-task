package epam.university.model.bouquet;

import java.util.ArrayList;
import java.util.List;

public class Bouquet implements IBouquet {

    private double price;
    private List<Flower> flowers;
    private List<BouquetDecorationElement> bouquetDecorationElements;

    public Bouquet(List<Flower> flowers) {
        this.flowers = flowers;
        this.bouquetDecorationElements = new ArrayList<>();
        countBouquetPrice();
    }

    public Bouquet() {
        this(new ArrayList<Flower>());
    }

    private void countBouquetPrice() {
        for (Flower flower : flowers) {
            price+=flower.getPrice();
        }
    }

    public double getPrice() {
        return price;
    } 

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<BouquetDecorationElement> getBouquetDecorationElements() {
        return bouquetDecorationElements;
    }

    public void setBouquetDecorationElements(List<BouquetDecorationElement> bouquetDecorationElements) {
        this.bouquetDecorationElements = bouquetDecorationElements;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "price=" + price +
                ", flowers=" + flowers +
                ", bouquetDecorationElements=" + bouquetDecorationElements +
                '}';
    }
}
