package epam.university.model.client;

import epam.university.model.discount.DiscountCardType;
import epam.university.model.discount.impl.BonusCard;
import epam.university.model.discount.impl.GoldCard;
import epam.university.model.discount.impl.RegularClientCard;

public enum SystemClient {
  OLGA("0673464819", new BonusCard()),
  YUNUS("0632114686", new GoldCard()),
  YURI("0633275156", new BonusCard()),
  IGOR("0667419697", new RegularClientCard());

  private String phoneNumber;
  private DiscountCardType discountCardType;

  SystemClient(String phoneNumber, DiscountCardType discountCardType) {
    this.phoneNumber = phoneNumber;
    this.discountCardType = discountCardType;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public DiscountCardType getDiscountCardType() {
    return discountCardType;
  }
}
