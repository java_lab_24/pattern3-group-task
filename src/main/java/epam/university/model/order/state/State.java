package epam.university.model.order.state;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.order.IOrder;

public interface State {

  default void getOrderStateName() {
    System.out.println("State");
  }

  default void bouquetCreation(IOrder order) {
    System.out.println("Bouquet creation is not allowed");
  }

  default void bouquetDecoration(IOrder order) {
    System.out.println("Decoration is not allowed");
  }

  default void registerOrder(IOrder order) {
    System.out.println("Register order is not allowed");
  }

  default void deliveringOrder(IOrder order) {
    System.out.println("Delivering order is not allowed");
  }
}
