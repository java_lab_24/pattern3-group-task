package epam.university.model.order.state.impl;

import epam.university.model.observer.delivery.DeliveryNotifier;
import epam.university.model.order.IOrder;
import epam.university.model.order.state.State;
import epam.university.model.observer.promoter.Promoter;
import java.util.Random;

public class OrderDeliveredState implements State {

  private Promoter promoter;
  private DeliveryNotifier deliveryNotifier;
  private final Random random = new Random();;
  private boolean promotionServerRun = true;
  private Thread promoterServer = new Thread(this::promotionServer);

  private static final int deliveryTime = 15000;

  @Override
  public void getOrderStateName() {
    System.out.println("DeliveredOrderState");
  }

  @Override
  public void deliveringOrder(IOrder order) {
    promoter = new Promoter();
    promoter.add(order.getClient());
    promoterServer.start();
    deliver(order);
  }

  private void deliver(IOrder order) {
    deliveryNotifier = new DeliveryNotifier();
    deliveryNotifier.add(order.getClient());
    try {
      Thread.sleep(deliveryTime);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    promotionServerRun = false;
    deliveryNotifier.notifySubscribers();
  }

  private void promotionServer() {
    while (promotionServerRun) {
      promoter.getPromotion();
      promoter.notifySubscribers();
      int sleepTime = 12000;
      while (sleepTime > 11000) {
        sleepTime = random.nextInt(50000);
      }
      try {
        Thread.sleep(sleepTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
