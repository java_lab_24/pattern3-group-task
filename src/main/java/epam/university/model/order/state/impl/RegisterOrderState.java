package epam.university.model.order.state.impl;

import epam.university.model.discount.DiscountConverter;
import epam.university.model.order.IOrder;
import epam.university.model.order.Order;
import epam.university.model.order.decorator.OrderDecorator;
import epam.university.model.order.decorator.delivery.CourierDelivery;
import epam.university.model.order.decorator.delivery.Delivery;
import epam.university.model.order.decorator.delivery.SpecialDelivery;
import epam.university.model.order.decorator.packaging.LightPackaging;
import epam.university.model.order.decorator.packaging.Packaging;
import epam.university.model.order.decorator.packaging.SafePackaging;
import epam.university.model.order.state.State;
import epam.university.view.View;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

public class RegisterOrderState implements State {

  private View view;
  private Scanner scn;
  private String userInput;

  private Order order;
  private OrderDecorator orderDecorator;

  public RegisterOrderState() {
    view = new View();
    scn = new Scanner(System.in);
  }

  @Override
  public void getOrderStateName() {
    System.out.println("RegisterOrderState");
  }

  @Override
  public void deliveringOrder(IOrder order) {
    this.order.setState(new OrderDeliveredState());
  }

  @Override
  public void registerOrder(IOrder order) {
    OrderDecorator orderDecorator = pack(order);
    orderDecorator = setDelivery(orderDecorator);
    DiscountConverter discountConverter = applyDiscount(orderDecorator);
    discountConverter.setOrder(orderDecorator);
    orderDecorator = discountConverter;
    this.order = (Order) order;
    this.order.setTotalPrice(orderDecorator.getCost());
    this.order.deliveringOrder();
  }

  private OrderDecorator pack(IOrder order) {
    view.printMenu(Arrays.asList("Light packaging", "Safe packaging"), "choose packaging type");
    Packaging packaging;
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    if (userInput.equals("1")) {
      packaging = new LightPackaging();
    } else {
      packaging = new SafePackaging();
    }
    packaging.setOrder(order);
    return packaging;
  }

  private OrderDecorator setDelivery(OrderDecorator order) {
    view.printMenu(Arrays.asList("Courier delivery", "Special delivery"), "choose delivery type");
    Delivery delivery;
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    if (userInput.equals("1")) {
      delivery = new CourierDelivery();
    } else {
      delivery = new SpecialDelivery();
    }
    delivery.setOrder(order);
    return delivery;
  }

  private DiscountConverter applyDiscount(OrderDecorator order) {
    return new DiscountConverter(order.getClient().getDiscountCard(),order);
  }
}
