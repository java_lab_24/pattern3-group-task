package epam.university.model.order.state.impl;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.bouquet.BouquetEnum;
import epam.university.model.bouquet.Flower;
import epam.university.model.bouquet.factory.BouquetMarketFactory;
import epam.university.model.bouquet.factory.impl.KyivBouquetMarketFactory;
import epam.university.model.bouquet.factory.impl.LvivBouquetMarketFactory;
import epam.university.model.bouquet.factory.impl.SimferopolBouquetMarketFactory;
import epam.university.model.order.IOrder;
import epam.university.model.order.Order;
import epam.university.model.order.state.State;
import epam.university.view.View;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class BouquetCreationState implements State {

  private View view;
  private Scanner scn;
  private String userInput;

  private Order order;
  private BouquetMarketFactory bouquetMarketFactory;


  public BouquetCreationState() {
    view = new View();
    scn = new Scanner(System.in);
  }

  @Override
  public void getOrderStateName() {
    System.out.println("BouquetCreationState");
  }

  @Override
  public void bouquetDecoration(IOrder order) {
    this.order.setState(new BouquetDecorationState());
  }

  @Override
  public void registerOrder(IOrder order) {
    this.order.setState(new RegisterOrderState());
  }

  @Override
  public void bouquetCreation(IOrder order) {
    this.order = (Order) order;
    initFactory();
    view.printBouquets(bouquetMarketFactory.getAvailableBouquets());
    BouquetEnum catalogBouquet = getCatalogBouquet(scn.nextLine());
    Bouquet bouquet;
    if (catalogBouquet != null) {
      bouquet = bouquetMarketFactory.getBouquet(catalogBouquet);
      this.order.setBouquet(bouquet);
      this.order.registerOrder();
    } else {
      this.order.setBouquet(getCustomBouquet());
      this.order.bouquetDecoration();
    }
  }

  private void initFactory() {
    view.printMenu(Arrays.asList("LvivMarket", "KyivMarket", "SimferopolMarket"),
        "choose your market");
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    while (bouquetMarketFactory == null) {
      switch (userInput) {
        case "1":
          bouquetMarketFactory = new LvivBouquetMarketFactory();
          break;
        case "2":
          bouquetMarketFactory = new KyivBouquetMarketFactory();
          break;
        case "3":
          bouquetMarketFactory = new SimferopolBouquetMarketFactory();
          break;
        default:
          System.out.println("market not found!");
          break;
      }
    }
  }

  private BouquetEnum getCatalogBouquet(String input) {
    List<BouquetEnum> factoryBouquets = bouquetMarketFactory.getAvailableBouquets();
    for (int i = 0; i < factoryBouquets.size(); i++) {
      if (Integer.parseInt(input) == (i + 1)) {
        return factoryBouquets.get(i);
      }
    }
    return null;
  }

  private Bouquet getCustomBouquet() {
    view.printFlowers(Arrays.asList(Flower.values()));
    int goToDecorationButton = Flower.values().length+1;
    List<Flower> userChosenFlowers = new LinkedList<>();
    while (!(userInput = scn.nextLine()).equals(Integer.toString(goToDecorationButton))) {
      if ((Integer.parseInt(userInput) >= 0)
          && (Integer.parseInt(userInput) < Flower.values().length)) {
        userChosenFlowers.add(Flower.values()[Integer.parseInt(userInput)-1]);
      }
      view.printFlowers(Arrays.asList(Flower.values()));
    }
    return new Bouquet(userChosenFlowers);
  }
}
