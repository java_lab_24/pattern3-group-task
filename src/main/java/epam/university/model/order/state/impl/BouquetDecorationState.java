package epam.university.model.order.state.impl;

import epam.university.model.bouquet.Bouquet;
import epam.university.model.bouquet.BouquetDecorationElement;
import epam.university.model.bouquet.decoration.designer.BouquetDecorator;
import epam.university.model.bouquet.decoration.event.EventEnum;
import epam.university.model.order.IOrder;
import epam.university.model.order.Order;
import epam.university.model.order.state.State;
import epam.university.view.View;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class BouquetDecorationState implements State {

  private View view;
  private Scanner scn;
  private String userInput;

  private BouquetDecorator bouquetDecorator;
  private Order order;

  public BouquetDecorationState() {
    view = new View();
    scn = new Scanner(System.in);
    bouquetDecorator = new BouquetDecorator();
  }

  @Override
  public void getOrderStateName() {
    System.out.println("BouquetDecorationState");
  }

  @Override
  public void registerOrder(IOrder order) {
    this.order.setState(new RegisterOrderState());
  }

  @Override
  public void bouquetDecoration(IOrder order) {
    this.order = (Order) order;
    decorate(this.order.getBouquet());
    this.order.registerOrder();
  }

  private Bouquet decorate(Bouquet bouquet) {
    view.printMenu(Arrays.asList("Decorate for event", "Custom decoration"), "input");
    if ((userInput = scn.nextLine()).equals("q")) {
      System.exit(0);
    }
    if (userInput.equals("1")) {
      return decorateToEvent(bouquet);
    } else {
      return decorateAsCustom(bouquet);
    }
  }

  private Bouquet decorateToEvent(Bouquet bouquet) {
    view.printMenu(Arrays.asList(EventEnum.values()), "choose event");
    userInput = scn.nextLine();
    while(true) {
      if ((Integer.parseInt(userInput) >= 0)
          && (Integer.parseInt(userInput) < EventEnum.values().length)) {
        bouquetDecorator.decorate(bouquet, EventEnum.values()[Integer.parseInt(userInput) - 1]);
        return bouquet;
      } else {
        System.out.println("\nevent not found!");
      }
    }
  }

  private Bouquet decorateAsCustom(Bouquet bouquet) {
    view.printDecorationElements(bouquetDecorator.getDecorationElements());
    List<BouquetDecorationElement> decorationElements = new LinkedList<>();
    while (Integer.parseInt(userInput = scn.nextLine())
        != bouquetDecorator.getDecorationElements().size()+1) {
      decorationElements.add(bouquetDecorator.getDecorationElements()
          .get(Integer.parseInt(userInput)));
      view.printDecorationElements(bouquetDecorator.getDecorationElements());
    }
    bouquet.setBouquetDecorationElements(decorationElements);
    return bouquet;
  }
}
