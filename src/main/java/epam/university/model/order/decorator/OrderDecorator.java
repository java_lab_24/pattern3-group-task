package epam.university.model.order.decorator;

import epam.university.model.client.Client;
import epam.university.model.discount.DiscountCard;
import epam.university.model.order.IOrder;
import java.util.Optional;

public class OrderDecorator implements IOrder, DiscountCard {

  private Optional<IOrder> order;
  private double additionalPrice;
  private String toName = "";
  private double deliveryPrice;


  public void setOrder(IOrder outOrder){
    order = Optional.ofNullable(outOrder);
  }

  public void setAdditionalPrice(double additionalPrice){
  this.additionalPrice = additionalPrice;
  }

  public void setToName(String toName){
    this.toName = toName;
  }
  public void setDeliveryPrice(double deliveryPrice){
    this.deliveryPrice = deliveryPrice;
  }

  @Override
  public double getCost() {
    return additionalPrice + order.orElseThrow(IllegalArgumentException::new).getCost();
  }

  @Override
  public String getName() {
    return order.orElseThrow(IllegalArgumentException::new).getName() + toName;
  }

  @Override
  public Client getClient() {
    return order.orElseThrow(IllegalArgumentException::new).getClient();
  }

  public double getOrderDeliveryPrice() {
    return this.deliveryPrice;
  }

  public Optional<IOrder> getOrder() {
    return order;
  }

  @Override
  public String toString() {
    return "OrderDecorator{" +
        "order=" + order +
        ", additionalPrice=" + additionalPrice +
        ", toName='" + toName + '\'' +
        '}';
  }

  @Override
  public double getPercentage() {
    return 0;
  }

  @Override
  public int getBonuses() {
    return 0;
  }
}
