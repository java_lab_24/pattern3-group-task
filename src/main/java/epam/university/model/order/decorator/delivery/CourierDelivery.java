package epam.university.model.order.decorator.delivery;

public class CourierDelivery extends Delivery {

  private final double PRICE = 10;
  private final String TO_NAME = " + CourierDelivery";


  public CourierDelivery(){
  setAdditionalPrice(PRICE);
  setToName(TO_NAME);
  setDeliveryPrice(PRICE);
  }
}
