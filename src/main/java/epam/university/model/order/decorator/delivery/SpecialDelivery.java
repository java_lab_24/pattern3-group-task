package epam.university.model.order.decorator.delivery;

public class SpecialDelivery extends Delivery {

  private final double PRICE = 50;
  private final String TO_NAME = " + SpecialDelivery";

  public SpecialDelivery(){
    setAdditionalPrice(PRICE);
    setToName(TO_NAME);
    setDeliveryPrice(PRICE);
  }
}
