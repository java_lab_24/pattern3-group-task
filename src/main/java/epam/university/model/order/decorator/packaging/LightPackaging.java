package epam.university.model.order.decorator.packaging;

public class LightPackaging extends Packaging {

  private final double PRICE = 10;
  private final String TO_NAME = " + LightPackaging";

  public LightPackaging(){
    setAdditionalPrice(PRICE);
    setToName(TO_NAME);
  }
}
